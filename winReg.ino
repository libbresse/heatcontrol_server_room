#include <dht.h>

dht DHT;

#define DHT11_PIN 7
#define fan_Pin 6


// constants won't change. Used here to 
// set pin numbers:
int iTemp;
const int iSetpoint = 20;
const int ledPin =  2;      // the number of the LED pin
const int ledPin2 =  3;
const int ledPin3 = 4;
// Variables will change:
int ledState = LOW;             // ledState used to set the LED
long previousMillis = 0;        // will store last time LED was updated

// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval = 900000;           // interval at which to blink (milliseconds)
int u =0;
void setup() {
  // set the digital pin as output:
  pinMode(ledPin, OUTPUT); 
  pinMode(ledPin2, OUTPUT);
  pinMode(fan_Pin, OUTPUT);
  delay(50); 
  pinMode(ledPin3, INPUT);
  delay(50);  
  digitalWrite(ledPin,HIGH);
  delay(50);
  digitalWrite(ledPin2,HIGH);
  delay(50);
  digitalWrite(ledPin3,HIGH);
  delay(50);
  Serial.begin(4800);
  Serial.println("DHT TEST PROGRAM ");
  Serial.print("LIBRARY VERSION: ");
  Serial.println(DHT_LIB_VERSION);
  Serial.println();
  Serial.println("Type,\tstatus,\tHumidity (%),\tTemperature (C)");
  previousMillis=0;
}

void loop()
{
  
   
  int chk = DHT.read11(DHT11_PIN);
  delay(1000);
  unsigned long currentMillis = millis();
  
  // ---------------fixar till previusmillis om timern har loopat runt, händer var 50e dag ungefär ---
  if(currentMillis+300000<previousMillis){
    previousMillis=currentMillis;
    Serial.println("Nollar-----");
    
  }
  // --------------------------------------- slut --------------------------------------------------- 
  
  Serial.println(currentMillis-previousMillis);
  iTemp=DHT.temperature;
  Serial.println(iTemp,1);
  int iError=(iSetpoint-DHT.temperature);
  Serial.println(iError);
  delay(1000);
  
  // ------------- starta fläkten om tempen blir för hög -----------------------------------
  if(iTemp>27){
      digitalWrite(fan_Pin,LOW); 
      Serial.println("FAN ON");
  }
  else if(iTemp<20){
      digitalWrite(fan_Pin,HIGH);
      Serial.println("FAN OFF");
  }
  else{
  }
  //-----------------------------------------------------------------------------------------
  
  if(currentMillis - previousMillis > interval && iError<-2) { // temp för hög
    previousMillis = currentMillis; 
    // --------- påslag och avslag knappen här är den i icke reglermode --------------------
    if(digitalRead(ledPin3)==true){  
      delay(50);  
      digitalWrite(ledPin,HIGH); 
      delay(50);
      digitalWrite(ledPin2,LOW);
      delay(50);
     }
     // -------------------------------------------------------------------------------------
    else {
      // save the last time you blinked the LED 
      digitalWrite(ledPin2,HIGH);
       // set the LED with the ledState of the variable:
      digitalWrite(ledPin, LOW);
      delay(3000);
      digitalWrite(ledPin, HIGH);
      delay(50);
    }
  }
  else if(currentMillis - previousMillis > interval && iError>2) { // temp för låg
    previousMillis = currentMillis;
    if(digitalRead(ledPin3)==true){  // påslag och avslag knappen
      delay(50);  
      digitalWrite(ledPin,HIGH);
      delay(50);
      digitalWrite(ledPin2,LOW);
      delay(50);
      
    }
    else{
      digitalWrite(ledPin,HIGH);
      // set the LED with the ledState of the variable:
      digitalWrite(ledPin2, LOW);
      delay(2000);
      digitalWrite(ledPin2, HIGH);
      delay(50);
      
    }
  }
  else if(currentMillis - previousMillis > interval){ // starta om klockan om tiden har gått ut och tempen är bra
    previousMillis = currentMillis;
    if(digitalRead(ledPin3)==true){  // påslag och avslag knappen
      delay(50);  
      digitalWrite(ledPin,HIGH);
      delay(50);
      digitalWrite(ledPin2,LOW);
      delay(50);
    }
  }
}

